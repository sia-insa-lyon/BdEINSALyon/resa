module.exports = {
    extends: ['stylelint-config-standard', 'stylelint-stylistic/config'],
    ignoreFiles: ['**/*.min.css', '**/*.min.js', '**/*.md'],
    rules: {
        'no-descending-specificity': null,
        'stylistic/indentation': 4,
        'stylistic/max-line-length': 125
    },
    overrides: [
        {
            files: ['*.html', '**/*.html', '**/*.js'],
            customSyntax: 'postcss-html'
        }
    ]
};
