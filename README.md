# Resa

----

## Description

C'est une application qui permet de gérer des réservations d'équipement.

La réservation doit se faire auprès d'une personne disposant de droits d'administration (typiquement : à l'accueil).

## Exécution locale

**Note** : Pour certaines personnes, il faut ajouter `sudo` au début des commandes.

### Prérequis 

**Linux** uniquement :
- Avoir Docker d'installé

**Windows**/**Linux** :
- Avoir Docker Desktop d'installé et de **lancé** 
Il y aura une erreur si ce dernier n'est pas actif quand vous exécutez.

### Lancement

En étant à la racine du dossier, exécutez la commande :
```shell
docker compose up
```

Le site est accessible à l'adresse `localhost:80`.

Puis quand vous avez terminé, exécutez :
```shell
docker compose down
```

### Création d'un super-utilisateur

Être super-utilisateur permet d'accéder à l'interface Admin pour créer des objets.

Pour en créer un il faut se connecter au terminal du container.

Sur Docker Desktop : utilisez le terminal du container `resa`.

Sans Docker Desktop, exécutez dans un autre terminal : 
```shell
docker exec -it resa sh
```

Une fois dans le terminal, exécutez :
```shell
pipenv run python manage.py createsuperuser
```

Créez ensuite le super-utilisateur en suivant les indications.

L'interface est ensuite accessible à l'adresse `localhost:80/admin`.

### Développement 

Si vous avez fait des modifications sur le code il faut reconstruire l'image avec :
```shell
docker compose build
```

## Ajouter des données

Cela se fait depuis l'interface Admin, il faut être [super-utilisateur](#création-dun-super-utilisateur) pour y accéder.

Il faut :
1. Créer une catégorie
2. Créer une place (= zone où est stocké le matériel)
3. Créer une ressource (= le dit matériel)

## API Adhésion
Adhésion utilise maintenant *Keycloak* pour l'authentification, donc il faut un peu changer les choses.
Les API adhésion doivent maintenant utiliser des *Clients keycloak* avec les *Service Accounts* activés.

Il leur faut le *Service Account Role* ``adhesion-api.staff``
(dans l'onglet le plus à droite, Client Roles > adhesion > staff > Add selected).

Il faut aussi montrer un *email*, sinon le plugin Keycloak de Django n'est pas content et renvoie une ``401``.
Pour régler ça, ajouter un *Mapper* comme suit
 * *Mapper Type* : `Hardcoded Claim`
 * *Token Claim Name* : `email`
 * *Claim value* : `resa@bde-insa-lyon.fr`
 * *Claim JSON Type* : `String`
et cochez la case *Add to userinfo*

## Licence

[![GNU GPL v3.0](http://www.gnu.org/graphics/gplv3-127x51.png)](http://www.gnu.org/licenses/gpl.html)

```
Resa - Manage and book resources
Copyright (C) 2016-2017 Gabriel AUGENDRE
Copyright (C) 2016-2017 BdE INSA Lyon

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
```

### Bugs
``dumpdata`` oublie le dernier **]**, il faut le remettre pour utilise `loaddata`