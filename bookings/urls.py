from django.urls import path, re_path
from django.views.generic import TemplateView

from bookings.views.bookings import (
    BookingDetailView,
    BookingUpdateView,
    BookingDeleteView,
    BookingCreateView,
)
from bookings.views.occurrences import (
    BookingOccurrenceCreateView,
    BookingOccurrenceUpdateView,
    BookingOccurrenceDeleteView,
)
from bookings.views.other import (
    ResourceCategoryDayView,
    CountableOccurrencesList,
    BookingFormView,
)
from bookings.views.api_va import api_va

app_name = "bookings"

urlpatterns = [
    path(
        "home/", TemplateView.as_view(template_name="bookings/home.html"), name="home"
    ),
    re_path(
        r"^resource-category/(?P<id>[0-9]+)$",
        ResourceCategoryDayView.as_view(),
        name="resource-category-day",
    ),
    re_path(r"^form/(?P<pk>[0-9]+)$", BookingFormView.as_view(), name="booking-form"),
    re_path(
        r"^booking/(?P<pk>[0-9]+)$", BookingDetailView.as_view(), name="booking-details"
    ),
    re_path(
        r"^booking/(?P<pk>[0-9]+)/edit$",
        BookingUpdateView.as_view(),
        name="booking-update",
    ),
    re_path(
        r"^booking/(?P<pk>[0-9]+)/delete$",
        BookingDeleteView.as_view(),
        name="booking-delete",
    ),
    path("booking/new", BookingCreateView.as_view(), name="booking-new"),
    re_path(
        r"^booking/(?P<booking_pk>[0-9]+)/occurrence/new$",
        BookingOccurrenceCreateView.as_view(),
        name="occurrence-new",
    ),
    re_path(
        r"^booking/(?P<booking_pk>[0-9]+)/occurrence/(?P<pk>[0-9]+)$",
        BookingOccurrenceUpdateView.as_view(),
        name="occurrence-edit",
    ),
    re_path(
        r"^booking/(?P<booking_pk>[0-9]+)/occurrence/(?P<pk>[0-9]+)/delete$",
        BookingOccurrenceDeleteView.as_view(),
        name="occurrence-delete",
    ),
    path("occurrences", CountableOccurrencesList.as_view(), name="occurrences_filter"),
    re_path(r"^api/va/(?P<va_key>c[0-9]+)$", api_va, name="api_va"),
    path("help", TemplateView.as_view(template_name="bookings/help.html"), name="help"),
]
