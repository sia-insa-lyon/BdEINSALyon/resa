/** Remplit les champs nom, prénom, email et téléphone avec une chaîne de caractères vide. */
function emptyFields() {
    document.getElementById('id_contact_first_name').value = '';
    document.getElementById('id_contact_last_name').value = '';
    document.getElementById('id_contact_email').value = '';
    document.getElementById('id_contact_phone').value = '';
}

/**
 * Remplit les champs nom, prénom, email et téléphone avec les informations de l'objet "student".
 * @param student objet récupéré depuis adhésion
 */
function fillFields(student) {
    document.getElementById('id_contact_first_name').value = student.first_name;
    document.getElementById('id_contact_last_name').value = student.last_name;
    document.getElementById('id_contact_email').value = student.email;
    document.getElementById('id_contact_phone').value = student.phone;
}

document.getElementById('va_form').addEventListener('submit', () => {
    // Fonction qui est appelée quand on appuie sur le bouton près du champ carte VA.
    // Elle permet de récupérer les données d'un utilisateur ayant la carte VA, afin de remplir les champs du formulaire de réservation.
    const vaButton = document.getElementById('va_button');
    const oldText = vaButton.textContent;
    // Désactive temporairement le bouton
    vaButton.setAttribute('disabled', 'disabled');
    vaButton.textContent = 'Chargement...';
    const vaCard = document.getElementById('va_card_input').value;

    // Récupère les données de la carte VA
    fetch(`/book/api/va/${vaCard}`)
        .then((response) => response.json())
        .then((response) => {
            document.getElementById('va-error-block').style.display = 'none';
            fillFields(response);
        })
        // Si jamais il y a une erreur quelconque
        .catch((err) => {
            let msg = err.message;
            if (err instanceof TypeError) {
                msg = 'Impossible de se connecter au serveur, vérifiez votre connexion !';
            }
            document.getElementById('va-error-block').style.display = 'block';
            document.getElementById('va-error-message').textContent = `ERREUR : ${msg}`;
            emptyFields();
        })
        .finally(() => {
            // In all cases, set the "Remplir" button usable again.
            vaButton.textContent = oldText;
            vaButton.removeAttribute('disabled');
        });
});
