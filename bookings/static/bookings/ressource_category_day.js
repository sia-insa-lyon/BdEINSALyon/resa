/**
 * Sert uniquement pour initialiser les popovers
 * @see {@link https://www.w3schools.com/bootstrap5/bootstrap_popover.php} Origine du code
 * @see {@link https://getbootstrap.com/docs/5.2/components/popovers/#enable-popovers} Explication
 */

const popoverTriggerList = Array.prototype.slice.call(document.querySelectorAll('[data-bs-toggle="popover"]'));
popoverTriggerList.map((popoverTriggerEl) => new bootstrap.Popover(popoverTriggerEl)); // eslint-disable-line no-undef
