/**
 * S'applique à la barre de recherche de matériel dans le formulaire d'occurrence.
*/

// Empêche l'utilisateur de valider par erreur le formulaire en appuyant sur la touche "Entrée" lorsqu'il se trouve dans la barre de recherche.
document.getElementById('searchResourcesInput').addEventListener('keydown', (event) => {
    if (event.key === 'Enter') {
        event.preventDefault(); // empêcher l'action de la touche "Entrée"
    }
});

/**
 * Permet de filtrer les ressources grâce à la barre de recherche.
 * Le filtrage est réalisé en cachant les lignes du tableau qui ne contiennent pas le contenu de la barre de recherche.
 * @see {@link https://www.w3schools.com/howto/howto_js_filter_table.asp} Inspiration du code
 * La fonction est référencée dans le fichier widgets.py de l'application bookings
 */

function searchResources() { // eslint-disable-line no-unused-vars
    const filter = document.getElementById('searchResourcesInput').value.toUpperCase();
    // Loop through all table rows, and hide those who don't match the search query
    Array.from(document.getElementById('searchResourcesTable').getElementsByTagName('tr'))
        .forEach((row) => {
            const td = row.getElementsByTagName('td')[0];
            if (td) {
                row.style.display = ((td.textContent || td.innerText).toUpperCase().indexOf(filter) > -1) // eslint-disable-line no-param-reassign
                    ? ''
                    : 'none';
            }
        });
}
