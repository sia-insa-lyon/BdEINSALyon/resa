import logging

from django.conf import settings
from oauthlib.oauth2 import BackendApplicationClient
from requests_oauthlib import OAuth2Session
from rest_framework import status
from rest_framework.exceptions import APIException

log = logging.getLogger(__name__)


class AdhesionAPI:
    client_id = settings.ADHESION_CLIENT_ID
    client_secret = settings.ADHESION_CLIENT_SECRET
    keycloak_url = settings.KEYCLOAK_ENDPOINT
    adhesion_url = settings.ADHESION_URL
    token = None
    client = None
    oauth = None

    def __init__(self):
        self.refresh_token()
        super().__init__()

    @classmethod
    def refresh_token(self, needs_refresh=False):
        if (self.token is None and self.client is None) or needs_refresh:
            if needs_refresh:
                log.info("Renouvellement du token car il est invalide")
            self.client = BackendApplicationClient(client_id=self.client_id)
            self.oauth = OAuth2Session(client=self.client)
            self.token = self.oauth.fetch_token(
                token_url=self.keycloak_url,
                client_id=self.client_id,
                client_secret=self.client_secret,
            )

    def get(self, url, **kwargs):
        import requests

        r = requests.get(
            url,
            headers={"Authorization": "Bearer " + self.token["access_token"]},
            **kwargs,
        )
        if r.status_code == 401:
            self.refresh_token(
                needs_refresh=True
            )  # visiblement y'a pas de refresh token avec une Application "client credentials"
            r = requests.get(
                url,
                headers={"Authorization": "Bearer " + self.token["access_token"]},
                **kwargs,
            )
        if r.status_code not in [
            status.HTTP_200_OK,
            status.HTTP_201_CREATED,
            status.HTTP_202_ACCEPTED,
            status.HTTP_404_NOT_FOUND,
        ]:
            raise APIException(
                "Une erreur est survenue lors de la connexion à un serveur externe"
            )
        return r

    def get_member(self, member_id):
        print(f"token JWT: {self.token['access_token']}")
        r = self.get(self.adhesion_url + "/v1/members/{}/".format(member_id))
        try:
            if r.status_code == 404:
                return None
            member = r.json()
            member["id"]
            return member
        except KeyError:  # keyerror si membre inexistant
            return None
        except AttributeError:  # pas de réponse
            return None

    def get_va(self, card_id):
        r = self.get(self.adhesion_url + "/v1/cards/{}/".format(card_id))
        try:
            if r.status_code == 404:
                return None
            card = r.json()
            if card["activated"]:
                member = self.get_member(card["member"])
                card["first_name"] = member["first_name"]
                card["last_name"] = member["last_name"]
                card["email"] = member["email"]
                card["phone"] = member["phone"]
            return card
        except KeyError:  # keyerror si membre inexistant
            return None
        except AttributeError:  # pas de réponse
            return None
