from django.contrib.auth.decorators import login_required
from django.http import JsonResponse

from bookings.adhesion import AdhesionAPI


@login_required
def api_va(request, va_key):
    api = AdhesionAPI()
    va_info = api.get_va(va_key)
    if va_info is None:
        return JsonResponse(dict(), status=404)
    va_info = {
        key: va_info[key] for key in ["first_name", "last_name", "email", "phone"]
    }
    return JsonResponse(va_info)
