import calendar
import datetime as dt
import re

from django.contrib.auth.decorators import login_required
from django.db.models import Q, Sum
from django.shortcuts import get_object_or_404
from django.utils.decorators import method_decorator
from django.views.generic import DetailView, ListView
from django.contrib.auth.mixins import UserPassesTestMixin

from bookings.models import (
    ResourceCategory,
    Resource,
    Booking,
    BookingOccurrence,
    OccurrenceResourceCount,
    ResourceLock,
)


class ResourceCategoryDayView(UserPassesTestMixin, ListView):
    template_name = "bookings/resource/resource_category_day.html"
    context_object_name = "resource_list"
    category = None
    decorators = []
    raise_exception = True

    def test_func(self):
        return self.category.public or self.request.user.is_authenticated

    def dispatch(self, request, *args, **kwargs):
        self.category = get_object_or_404(ResourceCategory, pk=kwargs["id"])

        if not self.category.public and len(self.decorators) == 0:
            self.decorators.append(login_required)
        elif len(self.decorators) > 0:
            self.decorators.pop()

        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        resources = Resource.objects.filter(category=self.category, available=True)
        if not self.request.user.is_authenticated:
            resources = resources.filter(public=True)
        return resources

    @method_decorator(decorators)
    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ResourceCategoryDayView, self).get_context_data(**kwargs)

        # Category
        context["category"] = self.category
        context["paragraphs"] = self.category.paragraphs.filter(
            order_public__gt=0
        ).order_by("order_public")

        # Date
        day, month, year = self._extract_date()
        date = dt.date(day=day, month=month, year=year)
        context["date"] = date

        context["today"] = dt.date.today()

        # Month calendar
        weeks = self._construct_month_calendar(month, year)
        context["weeks"] = weeks

        # Booking occurrences
        resources = self.get_queryset()

        lines = self._construct_day_view(date, resources)
        context["lines"] = lines

        return context

    def _construct_day_view(self, date, resources):
        lines = []
        was_booked_on_last_slot = []
        was_locked_on_last_slot = []

        # Keep a reference to the first cell of a series for a resource in order to increment the "rowspan" as needed
        first_cells = [{}] * resources.count()

        for slot in self.category.get_slots(date):
            line = {
                "slot": slot,
            }
            cells = []

            # Get all occurrences for the current slot
            slot_occurrences = (
                OccurrenceResourceCount.objects.filter(
                    resource__in=resources,
                    occurrence__start__lt=slot.end,
                    occurrence__end__gt=slot.start,
                )
                .values("resource")
                .annotate(count=Sum("count"))
                .values_list("resource", "count")
            )

            slot_occurrences_map = {k: v for (k, v) in slot_occurrences}

            # Get locks for our resources during this slot
            locks = ResourceLock.objects.filter(
                start__lt=slot.end, end__gt=slot.start, resources__in=resources
            ).distinct()
            locked_resources = Resource.objects.filter(
                id__in=locks.values("resources").distinct()
            ).values_list("id", flat=True)

            is_booked_on_current_slot = []
            is_locked_on_current_slot = []

            for index, resource in enumerate(resources):
                if resource.id in locked_resources:
                    is_locked_on_current_slot.append(resource.id)
                    lock = locks.get(resources=resource)
                    cell_continue = (
                        resource.id in was_locked_on_last_slot
                        and lock == first_cells[index]["lock"]
                    )

                    if cell_continue:
                        first_cells[index]["rowspan"] += 1
                    else:
                        cell = {
                            "type": "locked",
                            "lock": lock,
                            "resource": resource,
                            "rowspan": 1,
                        }
                        first_cells[index] = cell
                        cells.append(cell)

                elif resource.id in slot_occurrences_map:
                    is_booked_on_current_slot.append(resource.id)

                    # We need to separate behavior between countable and not countable
                    # because for not countable resources we want to show the beginning of each booking,
                    # not only the first and then everything in the same cell like countable resources.
                    # So for not countable resources, we still need to fetch the occurrences.
                    occurrences = BookingOccurrence.objects.filter(
                        resources=resource, start__lt=slot.end, end__gt=slot.start
                    )
                    if resource.id in was_booked_on_last_slot:
                        cell_continue = True
                    else:
                        cell_continue = (
                            "occurrences" in first_cells[index]
                            and occurrences.first()
                            == first_cells[index]["occurrences"][0]
                        )

                    if cell_continue:
                        first_cells[index]["rowspan"] += 1
                        first_cells[index]["occurrences"] |= occurrences
                        first_cells[index]["occurrences_id"] |= occurrences.values_list(
                            "id", flat=True
                        )
                    else:
                        cell = {
                            "type": "booked",
                            "count": resource.number
                            - slot_occurrences_map[resource.id],
                            "resource": resource,
                            "occurrences": []
                            if occurrences is None
                            else occurrences.all(),
                            "occurrences_id": []
                            if occurrences is None
                            else occurrences.values_list("id", flat=True),
                            "rowspan": 1,
                        }
                        first_cells[index] = cell
                        cells.append(cell)

                    cells.append(
                        {
                            "type": "count",
                            "count": resource.number
                            - slot_occurrences_map[resource.id],
                            "resource": resource,
                        }
                    )
                else:
                    cells.append(
                        {
                            "type": "free",
                            "count": resource.number,
                            "resource": resource,
                        }
                    )

            line["cells"] = cells
            lines.append(line)
            was_booked_on_last_slot = is_booked_on_current_slot
            was_locked_on_last_slot = is_locked_on_current_slot
        return lines

    @staticmethod
    def _construct_month_calendar(month, year):
        cal = calendar.Calendar()
        days_in_month = cal.itermonthdates(year, month)
        weeks = []
        week_index = 0
        for month_day in days_in_month:
            weeks.append([])
            weeks[week_index].append(month_day)
            if month_day.weekday() == 6:
                week_index += 1
        return weeks

    def _extract_date(self):
        try:
            day = int(self.request.GET.get("day", dt.date.today().day))
        except ValueError:
            day = dt.date.today().day

        try:
            month = int(self.request.GET.get("month", dt.date.today().month))
        except ValueError:
            month = dt.date.today().month

        try:
            year = int(self.request.GET.get("year", dt.date.today().year))
        except ValueError:
            year = dt.date.today().year

        while month > 12:
            year += 1
            month -= 12
        while month <= 0:
            year -= 1
            month += 12
        return day, month, year


class CountableOccurrencesList(ListView):
    template_name = "bookings/occurrence/occurrences_filter_list.html"
    model = BookingOccurrence
    filter = None
    context_object_name = "occurrences_list"

    def dispatch(self, request, *args, **kwargs):
        self.filter = map(int, self.request.GET.get("filter").split(","))
        return super(CountableOccurrencesList, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        if self.filter:
            return BookingOccurrence.objects.filter(pk__in=self.filter)
        else:
            return BookingOccurrence.objects.all()


class BookingFormView(DetailView):
    model = BookingOccurrence
    template_name = "bookings/booking/booking_form.html"
    occurrence = None

    def dispatch(self, request, *args, **kwargs):
        self.occurrence = get_object_or_404(BookingOccurrence, pk=self.kwargs["pk"])
        return super(BookingFormView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(BookingFormView, self).get_context_data(**kwargs)

        context["occurrence"] = self.occurrence
        context["booking"] = self.occurrence.booking

        category = self.occurrence.resources.first().category
        context["category"] = category
        context["paragraphs"] = category.paragraphs.filter(order_form__gt=0).order_by(
            "order_form"
        )

        context["total"] = {
            "fee": sum(map(lambda x: x.fee, self.occurrence.bookings.all())),
            "guarantee": sum(
                map(lambda x: x.guarantee, self.occurrence.bookings.all())
            ),
        }

        return context

    @method_decorator(login_required)
    def get(self, request, *args, **kwargs):
        return super(BookingFormView, self).get(request, *args, **kwargs)
