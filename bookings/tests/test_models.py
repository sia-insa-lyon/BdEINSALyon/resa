import datetime as dt

from django.test import TestCase

from bookings import models as m


class SlotTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        start = dt.datetime(2018, 1, 23, 8, 0)
        end = start.replace(hour=10)
        cls.slot = m.Slot(start=start, end=end)

    def test_equality(self):
        other_slot = m.Slot(start=self.slot.start, end=self.slot.end)
        self.assertEqual(other_slot, self.slot)


class ResourceCategoryTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.category = m.ResourceCategory.objects.create(
            name="testCategory",
            booking_type=m.ResourceCategory.INDIFFERENT,
            day_start=dt.time(10, 0),
            day_end=dt.time(14, 0),
            granularity=60,
            default_duration=60,
        )

    def test_get_slots(self):
        date = dt.date(2018, 1, 23)
        expected_slots = [
            m.Slot(
                start=dt.datetime(date.year, date.month, date.day, 10, 0),
                end=dt.datetime(date.year, date.month, date.day, 11, 0),
            ),
            m.Slot(
                start=dt.datetime(date.year, date.month, date.day, 11, 0),
                end=dt.datetime(date.year, date.month, date.day, 12, 0),
            ),
            m.Slot(
                start=dt.datetime(date.year, date.month, date.day, 12, 0),
                end=dt.datetime(date.year, date.month, date.day, 13, 0),
            ),
            m.Slot(
                start=dt.datetime(date.year, date.month, date.day, 13, 0),
                end=dt.datetime(date.year, date.month, date.day, 14, 0),
            ),
        ]

        found_slots = self.category.get_slots(date)

        self.assertEqual(found_slots, expected_slots)

    def test_get_slot(self):
        time = dt.datetime(2018, 1, 23, 11, 25)
        expected_slot = m.Slot(
            start=dt.datetime(2018, 1, 23, 11, 0), end=dt.datetime(2018, 1, 23, 12, 0)
        )
        found_slot = self.category.get_slot(time)
        self.assertEqual(found_slot, expected_slot)


class PlaceTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.place = m.Place.objects.create(name="testplace")

    def test_str(self):
        self.assertEqual(self.place.name, str(self.place))


class ResourceTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.category = m.ResourceCategory.objects.create(
            name="testCategory",
            booking_type=m.ResourceCategory.INDIFFERENT,
            day_start=dt.time(10, 0),
            day_end=dt.time(14, 0),
            granularity=60,
            default_duration=60,
        )

    def setUp(self):
        self.resource = m.Resource.objects.create(
            name="testResource", description="", category=self.category, number=1
        )
