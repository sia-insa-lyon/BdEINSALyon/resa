import datetime

from permissions.models import User
from django.test import TestCase
from django.urls import reverse
from bookings.models import (
    ResourceCategory,
    Booking,
    BookingOccurrence,
    Resource,
    OccurrenceResourceCount,
)


class MonthBeforeJanuaryTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.res = ResourceCategory.objects.create(
            name="test",
            booking_type="I",
            day_start=datetime.time(0, 0),
            day_end=datetime.time(0, 0),
            granularity=60,
            default_duration=60,
        )
        cls.months = {
            0: "décembre",
            -1: "novembre",
            -2: "octobre",
            -3: "septembre",
            -4: "août",
            -5: "juillet",
            -6: "juin",
            -7: "mai",
            -8: "avril",
            -9: "mars",
            -10: "février",
            -11: "janvier",
        }
        cls.day = 15

    def test_month_before_zero_are_valid(self):
        for i in range(0, -30, -1):
            response = self.client.get(
                reverse("bookings:resource-category-day", kwargs={"id": self.res.id}),
                {"month": i, "day": self.day},
            )
            self.assertLess(response.status_code, 400)

            expected_month = self.months[i % -12]
            self.assertIn(
                "{} - {} {}".format(self.res.name, self.day, expected_month),
                response.content.decode("utf-8"),
            )


class ResourceTestCases(TestCase):
    date_format = "%Y-%m-%d"
    time_format = "%H:%M"

    # Permet de récupérer le résultat du form
    def reverse_post(
        self,
        start: datetime,
        end: datetime,
        resource_1_number: int,
        resource_2_number: int,
        recurrence_type: str,
        recurrence_end: str,
    ):
        response = self.client.post(
            reverse("bookings:occurrence-new", kwargs={"booking_pk": self.booking.id}),
            {
                "start_0": start.date().strftime(self.date_format),
                "start_1": start.time().strftime(self.time_format),
                "end_0": end.date().strftime(self.date_format),
                "end_1": end.time().strftime(self.time_format),
                "recurrence_end": recurrence_end,
                "recurrence_type": recurrence_type,
                f"resources_{self.resource.id}": resource_1_number,
                f"resources_{self.resource2.id}": resource_2_number,
            },
        )
        return response

    @classmethod
    def setUpTestData(cls):
        cls.username = "testuser"
        cls.password = "djangooo"
        User.objects.create_superuser(
            username=cls.username, password=cls.password, email=""
        )

    def setUp(self):
        self.category = ResourceCategory.objects.create(
            name="test category",
            booking_type="I",
            day_start=datetime.time(8, 0),
            day_end=datetime.time(20, 0),
            granularity=60,
            default_duration=60,
        )
        self.resource = Resource.objects.create(
            name="test_resource",
            description="",
            category=self.category,
            number=2,
        )
        self.resource2 = Resource.objects.create(
            name="test_resource2",
            description="",
            category=self.category,
            number=2,
        )

        self.booking = Booking.objects.create(
            reason="test booking",
            details="test",
            contact_first_name="joe",
            contact_last_name="joe",
            contact_email="test@example.com",
            contact_phone="0699999999",
            contact_asso="asso",
        )
        self.occurrence1 = BookingOccurrence.objects.create(
            booking=self.booking,
            start=datetime.datetime(2018, 1, 23, 8),
            end=datetime.datetime(2018, 1, 23, 10),
        )
        self.occurrence2 = BookingOccurrence.objects.create(
            booking=self.booking,
            start=datetime.datetime(2018, 1, 23, 10),
            end=datetime.datetime(2018, 1, 23, 12),
        )
        OccurrenceResourceCount.objects.create(
            occurrence=self.occurrence1,
            resource=self.resource,
            count=1,
        )
        OccurrenceResourceCount.objects.create(
            occurrence=self.occurrence2,
            resource=self.resource,
            count=1,
        )

    def test_all_bookings_are_linked_when_countable(self):
        self.client.login(username=self.username, password=self.password)
        date = self.occurrence1.start
        response = self.client.get(
            reverse("bookings:resource-category-day", kwargs={"id": self.category.id}),
            {"year": date.year, "month": date.month, "day": date.day},
        )
        self.assertInHTML(
            '<a href="/book/occurrences?filter={},{}"><strong>Réservé</strong></a>'.format(
                self.occurrence1.id, self.occurrence2.id
            ),
            response.content.decode("utf-8"),
        )

    def test_occurrence_form_need_recurrence_end_error(self):
        self.client.login(username=self.username, password=self.password)
        start = datetime.datetime(2018, 1, 23, 14)
        end = datetime.datetime(2018, 1, 23, 15)
        response = self.reverse_post(start, end, 1, 1, "D", "")
        self.assertLess(response.status_code, 400)
        self.assertGreaterEqual(response.status_code, 200)
        self.assertFormError(
            response.context["form"],
            "recurrence_end",
            [
                "Vous devez saisir une date de fin si vous souhaitez ajouter une périodicité."
            ],
        )

    def test_occurrence_form_to_early_error(self):
        self.client.login(username=self.username, password=self.password)
        start = datetime.datetime(2018, 1, 23, 6)
        end = datetime.datetime(2018, 1, 23, 15)
        response = self.reverse_post(start, end, 1, 1, "N", "")
        self.assertLess(response.status_code, 400)
        self.assertGreaterEqual(response.status_code, 200)
        self.assertFormError(
            response.context["form"],
            "start",
            [
                f"La réservation ne peut pas commencer avant {self.category.day_start} pour la catégorie {self.category.name}"
            ],
        )

    def test_occurrence_form_too_late_error(self):
        self.client.login(username=self.username, password=self.password)
        start = datetime.datetime(2018, 1, 23, 12)
        end = datetime.datetime(2018, 1, 23, 23)
        response = self.reverse_post(start, end, 1, 1, "N", "")
        self.assertLess(response.status_code, 400)
        self.assertGreaterEqual(response.status_code, 200)
        self.assertFormError(
            response.context["form"],
            "end",
            [
                f"La réservation ne peut pas se terminer après {self.category.day_end} pour la catégorie {self.category.name}"
            ],
        )

    def test_occurrence_form_no_500_when_multiple_resource_errors(self):
        self.client.login(username=self.username, password=self.password)
        start = self.occurrence1.start
        end = self.occurrence1.end
        response = self.reverse_post(start, end, 3, 3, "N", "")
        self.assertLess(response.status_code, 400)
        self.assertGreaterEqual(response.status_code, 200)
        self.assertFormError(
            response.context["form"],
            "resources",
            [
                f"Seulement 1 {self.resource.name} disponible !",
                f"Seulement 2 {self.resource2.name} disponible !",
            ],
        )


class PrivateCategoryViewCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.user = User.objects.create_user(username="test", password="test")
        cls.category = ResourceCategory.objects.create(
            name="test category",
            booking_type="I",
            day_start=datetime.time(8, 0),
            day_end=datetime.time(20, 0),
            granularity=60,
            default_duration=60,
            public=False,
        )
        cls.category_id = cls.category.id

    def setUp(self):
        self.url = reverse(
            "bookings:resource-category-day",
            kwargs={"id": PrivateCategoryViewCase.category_id},
        )

    def test_user_not_authenticated(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 403)

    def test_user_authenticated(self):
        self.client.login(username="test", password="test")
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(
            response, "bookings/resource/resource_category_day.html"
        )
