import datetime
import logging

from django.core.mail import EmailMessage
from django.template.loader import get_template

from bookings.models import BookingOccurrence
from resa.settings import SERVER_EMAIL

log = logging.getLogger(__name__)


def send_reminder_email():
    """
    CRON Job triggered every 2 hours to send an email
    to people who made a reservation which ends in 1 to 3 hours from now
    """
    occ = BookingOccurrence.objects.filter(
        end__gte=datetime.datetime.now() + datetime.timedelta(hours=1),
        end__lte=datetime.datetime.now() + datetime.timedelta(hours=3),
    )
    emails_sent = 0

    for occurence in occ:
        try:
            content = (
                get_template("email/rappel.html").render(
                    {
                        "booking_reason": occurence.booking.reason,
                        "booking_resources": occurence.resources.all(),
                    }
                ),
            )

            message = EmailMessage(
                subject="Rappel de retour pour votre réservation",
                body=content,
                from_email=SERVER_EMAIL,
                to=[occurence.booking.contact_email],
            )

            message.content_subtype = "html"
            message.send(fail_silently=False)

            emails_sent += 1
        except Exception:
            log.exception("Erreur lors de l'envoi de l'email")
