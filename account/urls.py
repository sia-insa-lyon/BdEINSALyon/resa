"""accounts URL configuration
"""
from django.urls import path, re_path
from django.views.generic.base import TemplateView
from django.contrib.auth import views as auth_views

from account import views

urlpatterns = [
    re_path(
        r"^oauth/(?P<provider>[a-z_]*)/callback",
        views.OAuthCallback.as_view(),
        name="oauth_callback",
    ),
    re_path(
        r"^oauth/(?P<provider>[a-z_]*)/login",
        views.OAuthLogin.as_view(),
        name="oauth_login",
    ),
    path("", views.HomeLoginView.as_view(), name="home"),
    path("home", views.LoggedHomeView.as_view(), name="logged_home"),
    path("logout", auth_views.LogoutView.as_view(), name="logout"),
]
