FROM python:3.11-alpine

RUN apk add --no-cache tzdata bash && \
    cp /usr/share/zoneinfo/Europe/Paris /etc/localtime && \
    echo "Europe/Paris" >  /etc/timezone

RUN apk add --no-cache postgresql-dev gcc musl-dev

RUN apk add --no-cache gettext git

WORKDIR /app

RUN pip3 install pipenv
COPY Pipfile Pipfile.lock ./
RUN pipenv install

COPY . /app

VOLUME /app/staticfiles

ENV DATABASE_URL postgres://postgresql:postgresql@db:5432/resa

EXPOSE 8000

RUN chmod +x bash/run-prod.sh

RUN pipenv run python manage.py compilemessages -l en -l fr

CMD /app/bash/run-prod.sh
