# Demande de modification du code

## Sommaire

<!--- Rapide description de l'a fonctionalité à ajouter --->

## Contexte de l'utilisation

<!--- Décrivez votre besoin --->

## Changements à faire sur le site

<!---Décrivez comment vous voyez la solution, ajout de boutons, changement d'affichage, ajout d'une page dédiée... Mettez des exemples de ce qui peut se faire sur d'autres sites où faites un croquis sur papier ou sur paint. --->

## Impératifs et options

<!--- Si certains besoins sont prioritaires et d'autres plus accessoirs expliquez ça ici. Si les besoins sont vraiment séparables il vaux mieux créer un deuxième ticket faisant référérence à la première --->

## Commentaires

<!--- Ajoutez tous les éléments que vous pensez nécessaire.--->

## Quick actions
<!---Ne modifiez ce paragraphe que si vous savez ce que vous faites. Ces quick actions sont là pour faire en sorte que les tickets soient traitées de manière la plus efficace.--->

/label "Awaiting Feedback"
