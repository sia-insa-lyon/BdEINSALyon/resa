#!/bin/sh
yes yes | pipenv run python manage.py migrate && \
pipenv run python manage.py collectstatic --noinput && \
pipenv run python manage.py crontab add && \
pipenv run gunicorn resa.wsgi -b 0.0.0.0:8000 --log-file -
