module.exports = {
    env: {
        node: true,
        browser: true,
        es6: true
    },
    extends: [
        'airbnb-base'
    ],
    globals: {
        Atomics: 'readonly',
        SharedArrayBuffer: 'readonly'
    },
    parser: '@babel/eslint-parser',
    parserOptions: {
        ecmaVersion: 2021,
        requireConfigFile: false
    },
    plugins: [
        '@babel',
        'compat',
        'html'
    ],
    settings: {
        'html/html-extensions': ['.html'],
        'html/javascript-mime-types': ['text/javascript', 'application/javascript']
    },
    rules: {
        'comma-dangle': 'off',
        indent: [
            'error',
            4
        ],
        'max-len': [
            'warn',
            150
        ],
        'no-plusplus': 'off',
        'no-trailing-spaces': 'error',
        'no-param-reassign': 'warn',
        // Keep theses two following rules as long as:
        // - you keep JS code in your templates
        // - you don't use a bundler like webpack
        'no-undef': 'warn',
        'no-unused-vars': 'warn',
        semi: [
            'error',
            'always'
        ]
    },
    overrides: [
        {
            files: [
                '**/*.test.js'
            ],
            env: {
                jest: true
            },
            plugins: [
                'jest',
                'no-only-tests'
            ],
            rules: {
                'jest/no-disabled-tests': 'warn',
                'jest/no-focused-tests': 'error',
                'jest/no-identical-title': 'error',
                'jest/prefer-to-have-length': 'warn',
                'jest/valid-expect': 'error'
            }
        }
    ],
};
