module.exports = {
    collectCoverageFrom: [
        '**/*.js',
        '!**/node_modules/**',
        '!**/output/**',
        '!**/*.test.js',
        '!**/*.min.js'
    ],
    coverageReporters: [
        'text',
        'lcov',
        'cobertura'
    ],
    coverageDirectory: 'output',
    moduleNameMapper: {
        '^.+.(css|styl|less|sass|scss|png|jpg|ttf|woff|woff2)$': 'jest-transform-stub'
    },
    reporters: [
        'default',
        [
            'jest-junit', {
                suiteName: 'Javascript app tests',
                outputDirectory: 'output',
                outputName: 'junit_jest.xml',
                uniqueOutputName: false
            }
        ]
    ],
    /* When you start to write the first tests in order to filter the tests locations:
      - Uncomment the following block
      - Remove `--passWithNoTests` from the `test` CI script command line
    roots: [
        '<rootDir>/account/tests/js',
        '<rootDir>/bookings/tests/js',
        '<rootDir>/permissions/tests/js',
        '<rootDir>/test/js'
    ],
    */
    transform: {
        '^.+\\.[t|j]sx?$': 'babel-jest',
        '.+\\.(css|styl|less|sass|scss|png|jpg|ttf|woff|woff2)$': 'jest-transform-stub'
    },
    testEnvironment: 'jsdom',
    verbose: true
};
