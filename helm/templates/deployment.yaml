apiVersion: apps/v1
kind: Deployment
metadata:
  name: "{{ .Release.Name }}-django"
  labels:
    app: "{{ .Release.Name }}-django"
    {{- include "django.labels" . | nindent 4 }}
spec:
{{- if not .Values.autoscaling.enabled }}
  replicas: 1
{{- end }}
  strategy:
    type: Recreate
  selector:
    matchLabels:
      app: "{{ .Release.Name }}-django"
  template:
    metadata:
      name: "{{ .Release.Name }}-django"
      labels:
        app: "{{ .Release.Name }}-django"
    spec:
      containers:
        - name: "{{ .Release.Name }}-django"
          image: {{ .Values.image.repository | quote }}
          imagePullPolicy: {{ .Values.image.pullPolicy | quote }}
          ports:
            - name: django-http
              containerPort: 8000
              protocol: TCP
          livenessProbe:
            initialDelaySeconds: 120
            periodSeconds: 10
            httpGet:
              path: /
              port: django-http
              httpHeaders:
              - name: X-Health
                value:  "Kubernetes-Health"
              - name: Host
                value: "localhost"
          readinessProbe:
            initialDelaySeconds: 60
            periodSeconds: 5
            httpGet:
              path: /
              port: django-http
              httpHeaders:
              - name: X-Health
                value:  "Kubernetes-Health"
              - name: Host
                value: "localhost"
          resources:
            requests:
              cpu: {{ .Values.resources.django.requests.cpu | quote }}
              memory: {{ .Values.resources.django.requests.memory | quote }}
            limits:
              cpu: {{ .Values.resources.django.limits.cpu | quote }}
              memory: {{ .Values.resources.django.limits.memory | quote }}
          volumeMounts:
            - name: "{{ .Release.Name }}-volume"
              mountPath: /app/mediafiles
              subPath: mediafiles
            - name: "{{.Release.Name }}-volume"
              mountPath: /app/staticfiles
              subPath: staticfiles
          envFrom:
          - configMapRef:
              name: "{{ .Release.Name }}-configmap"
          - secretRef:
              name: "{{ .Release.Name}}-secret"
        - name: "{{ .Release.Name }}-nginx"
          image: nginx
          imagePullPolicy: {{ .Values.image.pullPolicy | quote }}
          ports:
            - name: nginx-http
              containerPort: 8080
              protocol: TCP
          livenessProbe:
            initialDelaySeconds: 120
            periodSeconds: 10
            httpGet:
              path: /healthcheck
              port: nginx-http
          readinessProbe:
            initialDelaySeconds: 30
            periodSeconds: 5
            httpGet:
              path: /healthcheck
              port: nginx-http
          resources:
            requests:
              cpu: {{ .Values.resources.nginx.requests.cpu | quote }}
              memory: {{ .Values.resources.nginx.requests.memory | quote }}
            limits:
              cpu: {{ .Values.resources.nginx.limits.cpu | quote }}
              memory: {{ .Values.resources.nginx.limits.memory | quote }}
          volumeMounts:
            - name: "{{.Release.Name }}-volume"
              mountPath: /app/staticfiles
              subPath: staticfiles
              readOnly: true
            - name: "{{ .Release.Name }}-configmap"
              mountPath: /etc/nginx/conf.d
      volumes:
        - name: "{{ .Release.Name }}-configmap"
          configMap:
            name: "{{ .Release.Name }}-configmap"
            items:
              - key: nginx.conf
                path: default.conf
        - name: "{{.Release.Name }}-volume"
          emptyDir: {}
